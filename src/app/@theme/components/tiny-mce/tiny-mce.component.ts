import {Component, OnDestroy, AfterViewInit, Output, EventEmitter, ElementRef} from '@angular/core';

@Component({
  selector: 'ngx-tiny-mce',
  template: '',
})
export class TinyMCEComponent implements OnDestroy, AfterViewInit {

  @Output() editorKeyup = new EventEmitter<any>();
  @Output() imageUploaded = new EventEmitter<any>();
  editor: any;

  constructor(private host: ElementRef) {
  }

  pickerCallback(cb, value, meta) {

    const input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');
    // Note: In modern browsers input[type="file"] is functional without
    // even adding it to the DOM, but that might not be the case in some older
    // or quirky browsers like IE, so you might want to add it to the DOM
    // just in case, and visually hide it. And do not forget do remove it
    // once you do not need it anymore.

    input.onchange = function () {
      const file = input.files[0];
      const reader = new FileReader();
      reader.onload = function () {
        // Note: Now we need to register the blob in TinyMCEs image blob
        // registry. In the next release this part hopefully won't be
        // necessary, as we are looking to handle it internally.
        const id = 'blobid' + (new Date()).getTime();
        const blobCache = tinymce.activeEditor.editorUpload.blobCache;
        const base64 = reader.result.split(',')[1];
        const blobInfo = blobCache.create(id, file, base64);
        blobCache.add(blobInfo);
        // call the callback and populate the Title field with the file name
        cb(blobInfo.blobUri(), {title: file.name});
      };
      reader.readAsDataURL(file);
    };
    input.click();
  }


  ngAfterViewInit() {
    tinymce.init({
      target: this.host.nativeElement,
      paste_data_images: true,
      image_advtab: true,
      plugins: ['link paste table image'],
      skin_url: 'assets/skins/lightgray',
      image_title: true,
      automatic_uploads: true,
      file_picker_types: 'image',
      file_picker_callback: this.pickerCallback,
      setup: editor => {
        this.editor = editor;
        editor.on('keyup', () => {

          this.editorKeyup.emit(editor.getContent());

          editor.editorUpload.scanForImages().then(res => {
            if (res.length) {
              this.imageUploaded.emit(res);
            }
          });
        });
      },
      height: '320',
    });
  }

  ngOnDestroy() {
    tinymce.remove(this.editor);
  }
}
