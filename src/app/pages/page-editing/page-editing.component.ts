import {Component, OnInit} from '@angular/core';
import {PageEditingService} from '../../@core/data/page-editing.service';
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';
import 'style-loader!angular2-toaster/toaster.css';
@Component({
  selector: 'ngx-page-editing',
  templateUrl: './page-editing.component.html',
  styleUrls: ['./page-editing.component.scss'],
})
export class PageEditingComponent implements OnInit {
  public aboutUs: any = '';
  public termsConditions: any = '';
  public config: ToasterConfig;
  public tabs: any[] = [
    {
      title: 'About Us',
      route: '/pages/page-editing/about-us',
    },
    {
      title: 'Terms & Conditions',
      route: '/pages/page-editing/terms',
    },
  ];

  public editorInit = {};

  constructor(public pageEditService: PageEditingService, private toasterService: ToasterService) {
    this.pageEditService.get({type: 'about-us'}).subscribe(res => {
      this.aboutUs = res['data'][0];
    });
    this.pageEditService.get({type: 'terms'}).subscribe(res => {
      this.termsConditions = res['data'][0];
    });

  }

  ngOnInit() {
    this.editorInit = {
      plugins: ['link paste table'],
      skin_url: 'assets/skins/lightgray',
      height: '320',
    };
  }
  public showToast (type: string, title: string, body: string) {
    this.config = new ToasterConfig({
      positionClass: 'toast-top-right',
      timeout: 5000,
      newestOnTop: true,
      tapToDismiss: true,
      preventDuplicates: false,
      animation: 'fade',
      limit: 5,
    });
    const toast: Toast = {
      type: type,
      title: title,
      body: body,
      timeout: 5000,
      showCloseButton: true,
      bodyOutputType: BodyOutputType.TrustedHtml,
    };
    this.toasterService.popAsync(toast);
  }

}

@Component({
  selector: 'ngx-tab1',
  template: `
    <nb-card>
      <nb-card-header>
        Edit "About Us" page
      </nb-card-header>
      <nb-card-body>

        <editor [init]="editorInit" [(ngModel)]="aboutUs.content"></editor>
      </nb-card-body>
      <nb-card-footer>
        <button class="btn btn-hero-primary" (click)="update()">Save</button>
      </nb-card-footer>
    </nb-card>
  `,
})
export class AboutUsComponent extends PageEditingComponent {

  update() {
    const data = {
      title: this.aboutUs.title,
      content: this.aboutUs.content,
      type: this.aboutUs.type,
      static_info_id: this.aboutUs.id,
    };
    this.pageEditService.update(data)
      .subscribe((res) => {
          if (res['status'] === 'success') {
            this.showToast('success', null, 'Page "About us" has been updated successfully');
          } else {
            this.showToast('error', null, res['message']);
          }
      });
  }
}

@Component({
  selector: 'ngx-tab2',
  template: `
    <nb-card>
      <nb-card-header>
        Edit "Terms & Conditions" page
      </nb-card-header>
      <nb-card-body>

        <editor [init]="editorInit" [(ngModel)]="termsConditions.content"></editor>
      </nb-card-body>
      <nb-card-footer>
        <button class="btn btn-hero-primary" (click)="update()">Save</button>
      </nb-card-footer>
    </nb-card>
  `,
})
export class TermsConditionsComponent extends PageEditingComponent {


  update() {
    const data = {
      title: this.termsConditions.title,
      type: this.termsConditions.type,
      content: this.termsConditions.content,
      static_info_id: this.termsConditions.id,
    };
    this.pageEditService.update(data)
      .subscribe(res => {
        if (res['status'] === 'success') {
          this.showToast('success', null, 'Page "Terms & Conditions" has been updated successfully');
        } else {
          this.showToast('error', null, res['message']);
        }
      });
  }
}
