import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import {PageEditingRoutingModule, routedComponents} from './page-editing.routing';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ToasterModule } from 'angular2-toaster';

@NgModule({
  imports: [
    ThemeModule,
    PageEditingRoutingModule,
    EditorModule,
    ToasterModule.forRoot(),
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
  ],
})
export class PageEditingModule { }
