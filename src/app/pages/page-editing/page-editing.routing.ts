import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {AboutUsComponent, PageEditingComponent, TermsConditionsComponent} from './page-editing.component';

const routes: Routes = [{
  path: '',
  component: PageEditingComponent,
  children: [{
    path: 'page-editing',
    component: PageEditingComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PageEditingRoutingModule { }

export const routedComponents = [
  PageEditingComponent,
  AboutUsComponent,
  TermsConditionsComponent,
];
