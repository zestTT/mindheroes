import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ToasterModule } from 'angular2-toaster';
import { UsersListRoutingModule, routedComponents } from './users-list.routing';
import {NbListModule} from '@nebular/theme';
import {MatDividerModule, MatIconModule, MatListModule} from '@angular/material';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {ButtonViewComponent} from './users-list.component';
@NgModule({
  imports: [
    ThemeModule,
    EditorModule,
    ToasterModule.forRoot(),
    UsersListRoutingModule,
    NbListModule,
    MatListModule,
    MatIconModule,
    MatDividerModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,
    ButtonViewComponent,
  ],
  providers: [
  ],
  entryComponents: [
    ButtonViewComponent,
  ],
})

export class UsersListModule { }
