import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {PagesComponent} from './pages.component';
import {AboutUsComponent, PageEditingComponent, TermsConditionsComponent} from './page-editing/page-editing.component';
import {AuthGuard} from '../@core/data/auth-guard.service';
import {ContentEditingComponent} from './content-editing/content-editing.component';
import {NbLogoutComponent} from '@nebular/auth';
import {UsersListComponent} from './users-list/users-list.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  canActivate: [AuthGuard],
  children: [
    {
      path: 'users-list',
      component: UsersListComponent,
    }, {
      path: 'content-list',
      component: ContentEditingComponent,
    }, {
      path: 'page-editing',
      component: PageEditingComponent,
      children: [{
        path: '',
        redirectTo: 'about-us',
        pathMatch: 'full',
      }, {
        path: 'about-us',
        component: AboutUsComponent,
      }, {
        path: 'terms',
        component: TermsConditionsComponent,
      }],
    }, {
      path: 'logout',
      component: NbLogoutComponent,
    }, {
      path: '',
      redirectTo: 'users-list',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
