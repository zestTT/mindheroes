import {NgModule} from '@angular/core';


import {PagesComponent} from './pages.component';
import {PagesRoutingModule} from './pages-routing.module';
import {ThemeModule} from '../@theme/theme.module';
import {PageEditingModule} from './page-editing/page-editing.module';
import { ContentEditingModule } from './content-editing/content-editing.module';
import { ComfirmationPromptComponent } from './shared/comfirmation-prompt/comfirmation-prompt.component';
import {MatButtonModule} from '@angular/material';
import {NbAuthModule} from '@nebular/auth';
import {ContentDialogComponent} from './content-editing/content-dialog/content-dialog.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {CalendarModule} from 'primeng/primeng';
import {UsersListModule} from './users-list/users-list.module';

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    PageEditingModule,
    ContentEditingModule,
    MatButtonModule,
    NbAuthModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    CalendarModule,
    UsersListModule,
  ],
  declarations: [
    ...PAGES_COMPONENTS,
    ComfirmationPromptComponent,
    ContentDialogComponent,
  ],
  entryComponents: [
    ComfirmationPromptComponent,
    ContentDialogComponent,
  ],
})
export class PagesModule {
}
