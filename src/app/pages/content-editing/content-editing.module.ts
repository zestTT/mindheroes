import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { EditorModule } from '@tinymce/tinymce-angular';
import { ToasterModule } from 'angular2-toaster';
import { ContentEditingRoutingModule, routedComponents } from './content-editing.routing';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {CalendarModule} from 'primeng/primeng';
@NgModule({
  imports: [
    ThemeModule,
    EditorModule,
    ToasterModule.forRoot(),
    ContentEditingRoutingModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    NgxMaterialTimepickerModule.forRoot(),
    CalendarModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
  ],
  entryComponents: [
  ],
})

export class ContentEditingModule { }
