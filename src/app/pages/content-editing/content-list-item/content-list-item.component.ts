import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ContentDialogComponent} from '../content-dialog/content-dialog.component';
import {ContentEditingService} from '../../../@core/data/content-editing.service';
import {ComfirmationPromptComponent} from '../../shared/comfirmation-prompt/comfirmation-prompt.component';

@Component({
  selector: 'ngx-content-list-item',
  templateUrl: './content-list-item.component.html',
  styleUrls: ['./content-list-item.component.scss'],
})
export class ContentListItemComponent implements OnInit {
  public image: any;
  @Input() content: any = {};
  @Output() contentDeleted = new EventEmitter<any>();
  constructor(public dialog: MatDialog,
              public contentService: ContentEditingService) {

  }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ContentDialogComponent, {
      data: {content: this.content, image: null},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.content = result.content;
        this.image = result.image;
        this.updateContent ();
      }
    });
  }

  updateContent () {
    const data = {
      name: this.content.name,
      date: this.content.date,
      time: this.content.time,
      content_id: this.content.id,
    };
    if (this.image)  data['image'] = this.image;
    this.contentService.update(data).subscribe(res => {
    });
  }
  deleteContent() {
    const dialogRef = this.dialog.open(ComfirmationPromptComponent, {
      data: this.content,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.contentService.delete({content_id: this.content.id}).subscribe(res => {
          this.contentDeleted.emit(this.content.id);
        });
      }
    });

  }
}
