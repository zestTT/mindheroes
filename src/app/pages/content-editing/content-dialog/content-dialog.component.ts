import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {DomSanitizer } from '@angular/platform-browser';
import {FormControl} from '@angular/forms';
import * as _moment from 'moment';

const moment = _moment;

interface Content {
  name: string;
  date: any;
  time: string;
  image: any;
}

@Component({
  selector: 'ngx-content-dialog',
  templateUrl: './content-dialog.component.html',
  styleUrls: ['./content-dialog.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ],
})
export class ContentDialogComponent implements OnInit {
  public content: Content = {
    name: '',
    date: null,
    time: '',
    image: null,
  };
  public name: string;
  public date: string;
  public time: string;
  public dateForm: any;
  public cacheDate: string;
  public validDate = true;
  public image: any;

  constructor(
    public sanitize: DomSanitizer,
    public dialogRef: MatDialogRef<ContentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
    for (const key in this.data.content) {
      if (this.data.content.hasOwnProperty(key)) {
        this.content[key] = this.data.content[key];
      }
    }
  }

  ngOnInit() {
    const parsedDate = this.content.date.split('.');
    this.dateForm = new FormControl(moment(parsedDate, 'DD/MM/YYYY'));
    this.content.time = this.content.time.slice(0, 5);
  }

  save() {
    if (this.validDate) {
      if (this.cacheDate) this.content.date = this.cacheDate;
      this.data.content = this.content;
      this.dialogRef.close({content: this.data.content, image: this.image});
    }
  }

  close() {
    this.dialogRef.close();
  }

  handleTime(event) {
    this.content.time = event.toLocaleTimeString('en-GB').slice(0, 5);
  }

  updateDate(date) {
    const value = date.targetElement.value;
    const parsedDate = moment(value).format('DD/MM/YYYY').split('/').join('.');
    if (this.dateValidator(value)) this.cacheDate = parsedDate;
  }

  dateValidator(dateCheck) {
    this.validDate = moment(moment(dateCheck).format('DD/MM/YYYY'), 'DD/MM/YYYY', true).isValid();

    return this.validDate;
  }

  dateHandler(event) {
    const value = event.target.value;
    const parsedDate = moment(value).format('DD/MM/YYYY').split('/').join('.');
    if (this.dateValidator(value)) this.cacheDate = parsedDate;
  }

  onFileChanged(event) {
    const file = event.target.files[0];
    const imageSrc = this.sanitize.bypassSecurityTrustResourceUrl(URL.createObjectURL(file));
    this.image = file;
    if (imageSrc) this.content.image = imageSrc;
  }
}
