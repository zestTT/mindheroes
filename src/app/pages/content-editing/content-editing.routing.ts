import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ContentEditingComponent} from './content-editing.component';
import {ContentListComponent} from './content-list/content-list.component';
import {ContentListItemComponent} from './content-list-item/content-list-item.component';
import {ContentDialogComponent} from './content-dialog/content-dialog.component';


const routes: Routes = [{
  path: '',
  component: ContentEditingComponent,
  children: [{
    path: 'content-editing',
    component: ContentEditingComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContentEditingRoutingModule {
}

export const routedComponents = [
  ContentEditingComponent,
  ContentListComponent,
  ContentListItemComponent,
];
