import {NbMenuItem} from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Users List',
    icon: 'nb-list',
    link: '/pages/users-list',
  },
  {
    title: 'Content List',
    icon: 'nb-edit',
    link: '/pages/content-list',
  },
  {
    title: 'Editing pages',
    icon: 'nb-compose',
    link: '/pages/page-editing',
    children: [
      {
        title: 'About-us',
        link: '/pages/page-editing/about-us',
      },
      {
        title: 'Terms & Conditions',
        link: '/pages/page-editing/terms',
      },
    ],
  },
];
