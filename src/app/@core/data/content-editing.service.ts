
import { of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { RequestService } from './request.service';



@Injectable()
export class ContentEditingService   {


  constructor (private request: RequestService) {
  }

  get() {
    const url = 'list-contents';

    return this.request.get(url);
  }

  create(data: any) {
    const url = 'create-static-information';
    const formData: FormData = new FormData();

    Object.keys(data).map(i => {
      formData.append(i, data[i]);
    });

    return this.request.post(url, formData);
  }

  update (data: any) {
    const url = 'update-content';
    const formData: FormData = new FormData();

    Object.keys(data).map(i => {
      formData.append(i, data[i]);
    });

    return this.request.post(url, formData);
  }

  delete (data: any) {
    const url = 'delete-content';

    return this.request.post(url, data);
  }
}
