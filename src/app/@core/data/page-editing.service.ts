
import { of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { RequestService } from './request.service';



@Injectable()
export class PageEditingService   {


  constructor (private request: RequestService) {
  }

  get(data) {
    const url = 'list-static-information';

    return this.request.post(url, data);
  }

  create(data: any) {
    const url = 'create-static-information';
    const formData: FormData = new FormData();

    Object.keys(data).map(i => {
      formData.append(i, data[i]);
    });

    return this.request.post(url, formData);
  }

  update (data: any) {
    const url = 'update-static-information';
    const formData: FormData = new FormData();

    Object.keys(data).map(i => {
      formData.append(i, data[i]);
    });

    return this.request.post(url, formData);
  }
}
