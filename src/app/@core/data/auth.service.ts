
import { of as observableOf,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { RequestService } from './request.service';



@Injectable()
export class AuthService   {


  constructor (private request: RequestService) {
  }

  getUser() {
    const url = 'get-user';

    return this.request.get(url);
  }


}
