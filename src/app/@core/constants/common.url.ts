const api = 'http://api.mind-hero.grassbusinesslabs.tk/api/';

export const COMMON_URL = {
  'page-editing': {
    get:                api + '/list-static-information',
    create:             api + '/create-static-information',
    update:             api + '/update-static-information',
    delete:             api + '/delete-static-information',
  },
  users: {
    index:                    api + '/users/',
  },
  jobs: {
    all:                      api + '/vacancy',
    create:                   api + '/vacancy',
  },
  job_category: {
    all:                      api + '/job_categories',
  },
  licence_list: {
    all:                      api + '/driving_licences',
  },
};
